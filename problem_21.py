#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Let d(n) be defined as the sum of proper divisors of n (numbers less than n
which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and
each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55
and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and
142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
"""

def get_divisors_sum(number):
	total = 0
	i = 1
	while i <= number / 2:
		if number % i == 0:
			total += i

		i += 1

	return total

amicable_found = {}

i = 1

while i < 10000:
	for_i = get_divisors_sum(i)
	if (for_i != i):
		for_i_result = get_divisors_sum(for_i)

		if (for_i_result == i):
			# they're amicable
			amicable_found[i] = for_i
			amicable_found[for_i] = for_i_result

	i += 1

total = 0
for k,v in amicable_found.iteritems():
	total += v

print total
