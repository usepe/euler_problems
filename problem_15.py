#!/usr/bin/python

"""
Starting in the top left corner of a 2×2 grid, there are 6 routes
(without backtracking) to the bottom right corner.

How many routes are there through a 20×20 grid?
"""

total = 0
n = 20

i = 2 * n
fact = 1
while i > 0:
	fact *= i
	i -= 1

total += fact

i = n
fact = 1
while i > 0:
	fact *= i
	i -= 1

total = total / (fact * fact)

print total
