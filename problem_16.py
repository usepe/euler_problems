#!/usr/bin/python

"""
21 ** 5 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 2 ** 1000?
"""

as_str = str(2 ** 1000)

total = 0

for number in as_str:
	total += int(number)

print total
