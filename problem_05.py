#!/usr/bin/python

"""
2520 is the smallest number that can be divided by each of the numbers from 1
to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the
numbers from 1 to 20?
"""

def divisible_by(number, divisor):
	if (number % divisor == 0):
		return True

	return False

def divisible_by_range(number):
	for i in range(1, 21):
		if not divisible_by(number, i):
			return False

	return True

done = False
i = 0

while not done:
	i += 1

	if divisible_by_range(i):
		done = True

print i
