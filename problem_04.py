#!/usr/bin/python

"""
A palindromic number reads the same both ways. The largest palindrome made from
the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""

def is_palindrome(number):
	as_str = str(number)
	char_count = len(as_str)
	middle = int(char_count / 2)

	i = 0

	while i <= middle:
		pos_left = i
		pos_right = char_count - i - 1

		if (as_str[pos_left] != as_str[pos_right]):
			return False

		i += 1

	return True

def get_max(collection):
	mayor = 0

	for number in collection:
		if number > mayor:
			mayor = number

	return mayor

palindroms = []

for i in range(1, 1000):
	for j in range(1, 1000):
		number = i * j
		if is_palindrome(number):
			palindroms.append(number)

print get_max(palindroms)
