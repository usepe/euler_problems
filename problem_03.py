#!/usr/bin/python

"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""

import math

def divisible_by(number, divisor):
	if (number % divisor == 0):
		return True

	return False

def is_prime(number):
	if number == 1:
		return True

	mults = 1

	for i in range(2, int(math.sqrt(number))):
		if divisible_by(number, i):
			mults += i

	if mults == 1:
		return True

	return False

number = 600851475143
baseNumber = number
primes = []
until = int(math.sqrt(baseNumber))

for i in range(1, until):
	if is_prime(i) and divisible_by(baseNumber, i):
		primes.append(i)

print primes

divisiblePrimes = []

for i in primes:
	if divisible_by(baseNumber, i):
		number = number / i

print divisiblePrimes

mayor = 1
total = 1
for prime in divisiblePrimes:
	if prime > mayor:
		mayor = prime

	total *= prime

if total == baseNumber:
	print mayor
else:
	print "No dio =/"
