#!/usr/bin/python

"""
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that
the 6th prime is 13.

What is the 10 001st prime number?
"""

import math

def divisible_by(number, divisor):
	if (number % divisor == 0):
		return True

	return False

def is_prime(number):
	if number == 1:
		return True

	mults = 1

	for i in range(2, int(math.sqrt(number)) + 1):
		if divisible_by(number, i):
			mults += 1

	if mults == 1:
		return True

	return False

actual_number = 1
primes_found = 0

while primes_found < 10001:
	actual_number += 1

	if is_prime(actual_number):
		primes_found += 1

print actual_number
