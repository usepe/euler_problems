#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains
10 terms. Although it has not been proved yet (Collatz Problem),
it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
"""

number = 0
max_iteration_for = 0
max_number = 0

while number < 1000000:
	number += 1

	print "Processing number " + str(number) + "..."

	seq = number

	iterations = 1

	while seq != 1:
		if seq % 2 == 0: # its even
			seq = seq / 2
		else:
			seq = (3 * seq) + 1

		iterations += 1

	if iterations > max_number:
		max_number = iterations
		max_iteration_for = number

print "Max number of iterations was for " + str(max_iteration_for) + "(Iterations: " + str(max_number) + ")"
