#!/usr/bin/python

"""
If the numbers 1 to 5 are written out in words: one, two, three, four, five,
then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out
in words, how many letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
forty-two) contains 23 letters and 115 (one hundred and fifteen) contains
20 letters. The use of "and" when writing out numbers is in compliance
with British usage.
"""

def getNumberAsString(i):
	numbers = {
		0: '',
		1: 'one',
		2: 'two',
		3: 'three',
		4: 'four',
		5: 'five',
		6: 'six',
		7: 'seven',
		8: 'eight',
		9: 'nine',
		10: 'ten',
		11: 'eleven',
		12: 'twelve',
		13: 'thirteen',
		14: 'fourteen',
		15: 'fifteen',
		16: 'sixteen',
		17: 'seventeen',
		18: 'eighteen',
		19: 'nineteen',
		1000: 'onethousand'
			 }

	from20to29 = 'twenty'
	from30to39 = 'thirty'
	from40to49 = 'forty'
	from50to59 = 'fifty'
	from30to90endedIn = 'ty'
	fromHundred = 'hundred'
	separator = 'and'

	if i < 20:
		as_str = numbers[i]
	elif i >= 20 and i < 30:
		aux = i - 20
		as_str = from20to29 + numbers[aux]
	elif i >= 30 and i < 40:
		aux = i - 30
		as_str = from30to39 + numbers[aux]
	elif i >= 40 and i < 50:
		aux = i - 40
		as_str = from40to49 + numbers[aux]
	elif i >= 50 and i < 60:
		aux = i - 50
		as_str = from50to59 + numbers[aux]
	elif i >= 60 and i < 100:
		append = from30to90endedIn
		if i >= 80 and i < 90:
			append = from30to90endedIn[1:]
		decena = int(str(i)[0:1])
		unidad = int(str(i)[1:2])
		as_str = numbers[decena] + append + numbers[unidad]
	elif i >= 100 and i < 1000:
		decenaUnidad = int(str(i)[1:])
		centena = int(str(i)[0:1])
		if decenaUnidad > 0:
			as_str = numbers[centena] + fromHundred + "and" + getNumberAsString(decenaUnidad)
		else:
			as_str = numbers[centena] + fromHundred
	else:
		as_str = numbers[i]

	return as_str

str_total = ''
for i in range(1, 1001):
	str_total += getNumberAsString(i)

print len(str_total)
