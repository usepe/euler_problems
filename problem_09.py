#!/usr/bin/python

"""
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a ** 2 + b ** 2 = c ** 2

For example, 3 ** 2 + 4 ** 2 = 9 + 16 = 25 = 5 ** 2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
"""

done = False

n = 0

while not done:
	n += 1

	done2 = False
	m = n + 1

	while not done2:
		m += 1

		a = m ** 2 - n ** 2
		b = 2 * m * n
		c = m ** 2 + n ** 2

		if a + b + c == 1000:
			triplet = (a,b,c)
			print "Founded: " + str(triplet) + ". Product: " + str(a * b * c)
		elif a + b + c > 1000:
			done2 = True

	if n > 1000:
		done = True
