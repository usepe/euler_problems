#!/usr/bin/python

"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
"""

import math

def divisible_by(number, divisor):
	if (number % divisor == 0):
		return True

	return False

def is_prime(number):
	if number == 1:
		return True

	mults = 1

	max_divisor = int(math.sqrt(number)) + 1

	for i in range(2, max_divisor):
		if divisible_by(number, i):
			mults += 1

	if mults == 1:
		return True

	return False

total = 0

for i in range(2, 2000001):
	if not i % 2 == 0:
		if (is_prime(i)):
			total += i

total += 2

print total
